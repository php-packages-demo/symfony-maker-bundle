# symfony/maker-bundle 

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/maker-bundle)
Generate the most common code you'll need in a Symfony app https://packagist.org/packages/symfony/maker-bundle

# Official documentation
* [*The Symfony MakerBundle*
  ](https://symfony.com/doc/current/bundles/SymfonyMakerBundle)
*  Symfony5: The Fast Track [*Step 6: Creating a Controller*
  ](https://symfony.com/doc/current/the-fast-track/en/6-controller.html)

# Unofficial documentation
* [*A Detailed Guide on Implementing Registration and Authentication in Symfony7(Part1)*
  ](https://medium.com/@ghaith.gtari/a-detailed-guide-on-implementing-registration-and-authentication-in-symfony7-part1-143b6e5cd446)
  2024-04 Ghaith Gtari

# Suggested related demo in [gitlab.com/php-packages-demo](https://gitlab.com/php-packages-demo)
## By "make" command
* **make:auth**                   Creates a Guard authenticator of different flavors
* **make:command**                Creates a new console command class
* **make:controller**             Creates a new controller class
  * symfony/twig-bundle (to generate template)
* **make:crud**                   Creates CRUD for Doctrine entity class
  * sensio/framework-extra-bundle
  * symfony/form
  * symfony/security-csrf
  * symfony/twig-bundle
  * symfony/validator
* **make:entity**                 Creates or updates a Doctrine entity class, and optionally an API Platform resource
  * [doctrine/doctrine-bundle
    ](https://gitlab.com/php-packages-demo/doctrine-bundle)
* **make:fixtures**               Creates a new class to load Doctrine fixtures
  * doctrine/doctrine-fixtures-bundle
* **make:form**                   Creates a new form class
* **make:functional-test**        Creates a new functional test class
* **make:migration**              Creates a new migration based on database changes
  * doctrine/doctrine-migrations-bundle
* **make:registration-form**      Creates a new registration form system
* **make:serializer:encoder**     Creates a new serializer encoder class
* **make:serializer:normalizer**  Creates a new serializer normalizer class
* **make:subscriber**             Creates a new event subscriber class
* **make:twig-extension**         Creates a new Twig extension class
* **make:unit-test**              Creates a new unit test class
* **make:user**                 Creates a new security user class
  * [symfony/security-bundle
    ](https://gitlab.com/php-packages-demo/symfony-security-bundle)
* **make:validator**              Creates a new validator and constraint class
* **make:voter**                  Creates a new security voter class

## By dependencies
* [doctrine/doctrine-bundle
    ](https://gitlab.com/php-packages-demo/doctrine-bundle)
  * **make:entity**                 Creates or updates a Doctrine entity class, and optionally an API Platform resource
* doctrine/doctrine-fixtures-bundle
  * **make:fixtures**               Creates a new class to load Doctrine fixtures
* doctrine/doctrine-migrations-bundle
  * **make:migration**              Creates a new migration based on database changes
* sensio/framework-extra-bundle
  * **make:crud**                   Creates CRUD for Doctrine entity class
* symfony/form
  * **make:crud**                   Creates CRUD for Doctrine entity class
* [symfony/security-bundle
  ](https://gitlab.com/php-packages-demo/symfony-security-bundle)
  * **make:user**                 Creates a new security user class
* symfony/security-csrf
  * **make:crud**                   Creates CRUD for Doctrine entity class
* symfony/twig-bundle
  * **make:controller** (to generate template)
  * **make:crud**                   Creates CRUD for Doctrine entity class
* symfony/validator
  * **make:crud**                   Creates CRUD for Doctrine entity class

### Dependencies by popularity
#### 89 doctrine/doctrine-bundle
<!--- 89 2020-07 -->
<!--- 92 2019-11 -->
[![PHPPackages Rank](http://phppackages.org/p/doctrine/doctrine-bundle/badge/rank.svg)](http://phppackages.org/p/doctrine/doctrine-bundle)
[![PHPPackages Referenced By](http://phppackages.org/p/doctrine/doctrine-bundle/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/doctrine-bundle)
* make:entity

#### 270 symfony/security-bundle
<!--- 270 2020-07 -->
<!--- 284 2019-11 -->
[![PHPPackages Rank](http://phppackages.org/p/symfony/security-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/security-bundle)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/security-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/security-bundle)
* make:user
* make:crud (symfony/security-csrf)

#### 287 doctrine/doctrine-migrations-bundle
<!--- 287 2020-07 -->
<!--- 309 2019-11 -->
[![PHPPackages Rank](http://phppackages.org/p/doctrine/doctrine-migrations-bundle/badge/rank.svg)](http://phppackages.org/p/doctrine/doctrine-migrations-bundle)
[![PHPPackages Referenced By](http://phppackages.org/p/doctrine/doctrine-migrations-bundle/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/doctrine-migrations-bundle)
* make:migration

#### 297 doctrine/doctrine-fixtures-bundle
<!--- 297 2020-07 -->
<!--- 303 2019-11 -->
[![PHPPackages Rank](http://phppackages.org/p/doctrine/doctrine-fixtures-bundle/badge/rank.svg)](http://phppackages.org/p/doctrine/doctrine-fixtures-bundle)
[![PHPPackages Rank](http://phppackages.org/p/doctrine/doctrine-fixtures-bundle/badge/rank.svg)](http://phppackages.org/p/doctrine/doctrine-fixtures-bundle)
* make:fixtures

#### make:crud
* 135 <!--- symfony/validator -->
  <!--- 135 2020-07 -->
  <!--- 145 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/validator/badge/rank.svg)](http://phppackages.org/p/symfony/validator)
  symfony/validator
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/validator/badge/referenced-by.svg)](http://phppackages.org/p/symfony/validator)
* 136 <!--- sensio/framework-extra-bundle -->
  <!--- 136 2020-07 -->
  <!--- 137 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/sensio/framework-extra-bundle/badge/rank.svg)](http://phppackages.org/p/sensio/framework-extra-bundle)
  sensio/framework-extra-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/sensio/framework-extra-bundle/badge/referenced-by.svg)](http://phppackages.org/p/sensio/framework-extra-bundle)
* 138 <!--- symfony/form -->
  <!--- 138 2020-07 -->
  <!--- 150 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/form/badge/rank.svg)](http://phppackages.org/p/symfony/form)
  symfony/form
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/form/badge/referenced-by.svg)](http://phppackages.org/p/symfony/form)
* 204 <!--- symfony/twig-bundle -->
  <!--- 204 2020-07 -->
  <!--- 216 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/twig-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/twig-bundle)
  symfony/twig-bundle
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/twig-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/twig-bundle)
* 771 <!--- symfony/security-csrf -->
  <!--- 771 2020-07 -->
  <!--- 889 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/security-csrf/badge/rank.svg)](http://phppackages.org/p/symfony/security-csrf)
  symfony/security-csrf
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/security-csrf/badge/referenced-by.svg)](http://phppackages.org/p/symfony/security-csrf)


