(
    install -d $1
    cd $1
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "exit(trim(file_get_contents('https://composer.github.io/installer.sig')) === hash_file('SHA384', 'composer-setup.php')?0:1);"
    php composer-setup.php --quiet
    rm composer-setup.php
    ln -s composer.phar composer
)
PATH=$PATH:$1
